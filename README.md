Si4703 Arduino Library
======================

Library for Sparkfun Si4703 FM Receiver (tuner) breakout board.

By Andrew Wyatt
retrojdm.com

Started:        2013-09-18
Updated:        2013-09-19
Last Change:    frequencyToChannel() and channelToFrequency()

--------------------------------------------------------------------------------

Extending the work of both:
* Simon Monk, and
* Nathan Seidle from Sparkfun


I initially just wanted to fiddle with the seek sensitivity settings, and show the channel while seeking.

After reading the datasheet I got a bit carried away.

A big thankyou goes to Nathan and Simon. I wouldn't have even attempted to use the Si4703 without their code.

Note: My code formatting and comments rely hevily on a tab width of 4 characters. Otherwise it will look messy as hell.


Simon's comments
----------------

Library for Sparkfun Si4703 breakout board.

Simon Monk. 2011-09-09

This is a library wrapper and a few extras to the excellent code produced by Nathan Seidle from Sparkfun (Beerware).


Nathan's comments
-----------------

Look for serial output at 57600bps.

The Si4703 ACKs the first byte, and NACKs the 2nd byte of a read.

1/18 - after much hacking, I suggest NEVER write to a register without first reading the contents of a chip. ie, don't updateRegisters without first readRegisters.



If anyone manages to get this datasheet downloaded

http://wenku.baidu.com/view/d6f0e6ee5ef7ba0d4a733b61.html



Please let us know. It seem to be the latest version of the programming guide. It had a change on page 12 (write 0x8100 to 0x07) that allowed me to get the chip working.

Also, if you happen to find "AN243: Using RDS/RBDS with the Si4701/03", please share. I love it when companies refer to documents that don't exist.

1/20 - Picking up FM stations from a plane flying over Portugal! Sweet! 93.9MHz sounds a little soft for my tastes,s but it's in Porteguese.

ToDo:
* Display current status (from 0x0A) (done 1/20/11)
* Add RDS decoding (works, sort of)
* Volume Up/Down (done 1/20/11)
* Mute toggle (done 1/20/11)
* Tune Up/Down (done 1/20/11)
* Read current channel (0xB0) (done 1/20/11)
* Setup for Europe (done 1/20/11)
* Seek up/down (done 1/25/11)

The Si4703 breakout does work with line out into a stereo or other amplifier. Be sure to test with different length 3.5mm cables. Too short of a cable may degrade reception.