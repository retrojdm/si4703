#include "Arduino.h"
#include "Si4703.h"
#include "Wire.h"



Si4703::Si4703(int resetPin, int sdioPin, int sclkPin) {
    _resetPin   = resetPin;
    _sdioPin    = sdioPin;
    _sclkPin    = sclkPin;
}



void Si4703::powerOn() {
    si4703_init();
}


void Si4703::powerOff() {
    digitalWrite(_resetPin, LOW);
}



void Si4703::setVolume(int volume) {

    readRegisters();
    setRegValue(REG_SYSCONFIG2, VOLUME, constrain(volume, 0, 15));
    updateRegisters();
}



void Si4703::setFrequency(int freq_MHz_x100) {

    /*
    Eg: freq_MHz_x_100 = 9770; // 97.7 Mhz
    */

    readRegisters();

    // Convert the Frequency to a "channel".
    int intChannel = frequencyToChannel(freq_MHz_x100);

    setRegValue(REG_CHANNEL, CHAN, intChannel);     // Mask in the new channel
    setRegValue(REG_CHANNEL, TUNE, 1);              // Set the TUNE bit to start

    updateRegisters();                              // Go :)

    delay(60);                                      // Wait 60ms - you can use or skip this delay

    // Poll to see if STC is set
    while(!seekTuneComplete());

    readRegisters();
    setRegValue(REG_CHANNEL, TUNE, 0);              // Clear the tune after a tune has completed.
    updateRegisters();

    // Wait for the si4703 to clear the STC as well
    while(seekTuneComplete());

    readRegisters();
    stereo = (boolean)getRegValue(REG_STATUSRSSI, STEREO); // Stereo?
}



int Si4703::getFrequency() {

    // Returns the frequency in MHz x 10.
    // So, a returned value of 977 = 97.7 MHz.
    
    readRegisters();

    // Get the channel
    int intChannel  = getRegValue(REG_READCHAN, READCHAN);
    stereo          = (boolean)getRegValue(REG_STATUSRSSI, STEREO); 

    // Convert to Frequency in MHz x 10
    return channelToFrequency(intChannel);
}



// These are the original SEEK functions that wait for STC before continuing.
int     Si4703::seekUp()            { seek(SEEK_UP,     true); return seekStatus(); }
int     Si4703::seekDown()          { seek(SEEK_DOWN,   true); return seekStatus(); }



// The startSeeking...() functions tell the Si4703 to start seeking, then let you go off and do your own thing.
// In your loop, you can check getFrequency() to display the frequency as the Si4703 is seeking :)
// It's your responsibility to keep checking seekTuneComplete() until it returns true.
// You then need to call seekStatus();
void    Si4703::startSeekingUp()    { seek(SEEK_UP,     false); }
void    Si4703::startSeekingDown()  { seek(SEEK_DOWN,   false); }
boolean Si4703::seekTuneComplete()  { readRegisters(); return (boolean)getRegValue(REG_STATUSRSSI, STC); }

int Si4703::seekStatus() {      
    // Returns 0 if the seek failed or reached a band limit.
    // Returns the frequency if seeking was successful.

    readRegisters();
    int valueSFBL = getRegValue(REG_STATUSRSSI, SFBL);  // Store the value of SFBL
    setRegValue(REG_POWERCFG, SEEK, 0);                 // Clear the seek bit after seek has completed
    updateRegisters();

    // Wait for the si4703 to clear the STC as well
    do { } while (seekTuneComplete());

    if(valueSFBL) { // The bit was set indicating we hit a band limit or failed to find a station
        return(0);
    }
    
    return getFrequency();
}


void Si4703::readRDS(char* buffer, long timeout) { 
    
    long endTime = millis() + timeout;
    boolean completed[] = {false, false, false, false};
    int completedCount = 0;

    while(completedCount < 4 && millis() < endTime) {
        readRegisters();
        if(getRegValue(REG_STATUSRSSI, RDSR)) {
            // ls 2 bits of B determine the 4 letter pairs
            // once we have a full set return
            // if you get nothing after 20 readings return with empty string
            uint16_t b = si4703_registers[REG_RDSB];
            int index = b & 0x03;
            if (! completed[index] && b < 500) {
                completed[index] = true;
                completedCount ++;
                char Dh = (si4703_registers[REG_RDSD] & 0xFF00) >> 8;
                char Dl = (si4703_registers[REG_RDSD] & 0x00FF);
                buffer[index * 2] = Dh;
                buffer[index * 2 +1] = Dl;

                // Serial.print(si4703_registers[REG_RDSD]); Serial.print(" ");
                // Serial.print(index);Serial.print(" ");
                // Serial.write(Dh);
                // Serial.write(Dl);
                // Serial.println();
            }
            delay(40); // Wait for the RDS bit to clear

        } else {
            delay(30); // From AN230, using the polling method 40ms should be sufficient amount of time between checks
        }
    }
    
    if (millis() >= endTime) {
        buffer[0] ='\0';
        return;
    }

    buffer[8] = '\0';
}




void Si4703::si4703_init() {
    
    // To get the Si4703 inito 2-wire mode, SEN needs to be high and SDIO needs to be low after a reset
    // The breakout board has SEN pulled high, but also has SDIO pulled high. Therefore, after a normal power up
    // The Si4703 will be in an unknown state. RST must be controlled

    // +------------+
    // | Set up I2C |
    // +------------+
    pinMode         (_resetPin, OUTPUT);
    pinMode         (_sdioPin,  OUTPUT);            // SDIO is connected to A4 for I2C
    digitalWrite    (_sdioPin,  LOW);               // A low SDIO indicates a 2-wire interface
    digitalWrite    (_resetPin, LOW);               // Put Si4703 into reset
    delay           (1);                            // Some delays while we allow pins to settle
    digitalWrite    (_resetPin, HIGH);              // Bring Si4703 out of reset with SDIO set to low and SEN pulled high with on-board resistor
    delay           (1);                            // Allow Si4703 to come out of reset
    Wire.begin();                                   // Now that the unit is reset and I2C inteface mode, we need to begin I2C
    
    // Enable the Crystal Oscillator
    readRegisters();                                // Read the current register set
    setRegValue(REG_TEST1,      XOSCEN, 1);         // Crystal Oscillator Enabled
    updateRegisters();
    delay(500);                                     // Wait for clock to settle - from AN230 page 9
    


    readRegisters();                                // Read the current register set

    // +---------------+
    // | Volume / Mute |
    // +---------------+
    setRegValue(REG_POWERCFG,   ENABLE, 1);         // Enable the IC
    setRegValue(REG_POWERCFG,   DMUTE,  1);         // Disable Mute
    setRegValue(REG_SYSCONFIG2, VOLUME, 1);         // Set volume to lowest

    
    // +--------+
    // | Region |
    // +--------+
    setRegValue(REG_SYSCONFIG2, BAND,   0);         // 0 = 87.5�108 MHz (USA, Europe) (Default).
                                                    // 1 = 76�108 MHz (Japan wide band).
                                                    // 2 = 76�90 MHz (Japan).

    setRegValue(REG_SYSCONFIG1, DE,     1);         // 0 = 70us. Used in USA (default).
                                                    // 1 = 50us. Used in Europe, Australia, Japan.

    setRegValue(REG_SYSCONFIG2, SPACE,  0);         // 0 = 200 kHz (USA, Australia) (default).
                                                    // 1 = 100 kHz (Europe, Japan).
                                                    // 2 =  50 kHz. NOT SUPPORTED IN THIS LIBRARY
                                                    //              Since we're storing frequencies as integers in MHz x 10...
                                                    //              97.75 MHz would end up being 977.5
                                                    //              And obviously integers can't have decimal points ;)
                                                        

    // +---------------------------+
    // | Seek Mode and Sensitivity |
    // +---------------------------+
    // See AN284 - Part 6. Si4700/01 Seek Settings Recommendation
    setRegValue(REG_POWERCFG,   SKMODE, 0);         // Allow wrap - if you disallow wrap, you may want to tune to 87.5 first
    setRegValue(REG_SYSCONFIG2, SEEKTH, 0x00);      // RSSI Seek Threshold                  (0x00 - 0x7F)
    setRegValue(REG_SYSCONFIG3, SKSNR,  0x04);      // Seek SNR Threshold                   (0x00 - 0x0F)
    setRegValue(REG_SYSCONFIG3, SKCNT,  0x0F);      // Seek FM Impulse Detection Threshold  (0x00 - 0x0F)


    // +-----+
    // | RDS |
    // +-----+
    setRegValue(REG_SYSCONFIG1, RDS,    1);         // 0 = Disable RDS (default).
                                                    // 1 = Enable RDS.
    

    updateRegisters();                              // Update
    delay(110);                                     // Max powerup time, from datasheet page 13
}




int trailingZeroes(unsigned int v) {
    
    // Returns the position of the least significant set bit.
    // Eg: 0x3C00 in Hex = 0011 1100 0000 0000 in binary. The result would be 10 trailing zeroes.

    // This function is used by the getRegValue() and setRegValue() functions.

    static const int MultiplyDeBruijnBitPosition[32] = {
         0,  1, 28,  2, 29, 14, 24,  3, 30, 22, 20, 15, 25, 17,  4,  8, 
        31, 27, 13, 23, 21, 19, 16,  7, 26, 12, 18,  6, 11,  5, 10,  9
    };
    return MultiplyDeBruijnBitPosition[((uint32_t)((v & -v) * 0x077CB531U)) >> 27];
}



int Si4703::getRegValue(uint16_t intRegister, uint16_t intMask) {
    
    // Masks the register, then shifts it right to give you the value.
    //
    // We can get away with requiring only the mask as a parameter, because the amount to shift
    // can be derrived from the mask by using the trailingZeroes() function.
    
    return (si4703_registers[intRegister] & intMask) >> trailingZeroes(intMask);
}



int Si4703::setRegValue(uint16_t intRegister, uint16_t intMask, uint16_t value) {

    // Clears the bits in the register where the mask is set,
    // then shifts the value to the right and slaps it down where it need to be ;)
    // 
    // We can get away with requiring only the mask as a parameter, because the amount to shift
    // can be derrived from the mask by using the trailingZeroes() function.

    si4703_registers[intRegister] &= ~intMask;                              // Clear the mask bits
    si4703_registers[intRegister] |= (value << trailingZeroes(intMask));    // set the bits
}



void Si4703::readRegisters(){

    // Read the entire register control set from 0x00 to 0x0F
    // Si4703 begins reading from register upper register of 0x0A and reads to 0x0F, then loops to 0x00.

    Wire.requestFrom(SI4703, 32);   // We want to read the entire register set from 0x0A to 0x09 = 32 bytes.

    while(Wire.available() < 32) ;  // Wait for 16 words (32 bytes) to come back from slave I2C device
                                    // We may want some time-out error here

    // Remember, register 0x0A comes in first so we have to shuffle the array around a bit
    for(int x = 0x0A ; ; x++) {                 // Read in these 32 bytes
        if(x == 0x10) x = 0;                    // Loop back to zero
        si4703_registers[x] = Wire.read() << 8;
        si4703_registers[x] |= Wire.read();
        if(x == 0x09) break;                    // We're done!
    }
}



byte Si4703::updateRegisters() {
    
    // Write the current 9 control registers (0x02 to 0x07) to the Si4703
    // It's a little weird, you don't write an I2C address
    // The Si4703 assumes you are writing to 0x02 first, then increments

    Wire.beginTransmission(SI4703);
    
    // A write command automatically begins with register 0x02 so no need to send a write-to address
    // First we send the 0x02 to 0x07 control registers
    // In general, we should not write to registers 0x08 and 0x09
    for(int regSpot = 0x02 ; regSpot < 0x08 ; regSpot++) {
        byte high_byte  = si4703_registers[regSpot] >> 8;
        byte low_byte   = si4703_registers[regSpot] & 0x00FF;

        Wire.write(high_byte);  // Upper 8 bits
        Wire.write(low_byte);   // Lower 8 bits
    }

    // End this transmission
    byte ack = Wire.endTransmission();
    if (ack != 0) { // We have a problem! 
        return(FAIL);
    }

    return(SUCCESS);
}



void Si4703::seek(byte seekDirection, boolean bWaitForSTC) {

    // Seeks out the next available station
    // Set bWaitForSTC to true if you want to wait for the STC bit to be set

    stereo = false;
    
    readRegisters();    
    setRegValue(REG_POWERCFG, SEEKUP, seekDirection == SEEK_UP ? 1 : 0);    // Seeking up or down?
    setRegValue(REG_POWERCFG, SEEK, 1);                                     // Set the SEEK bit
    updateRegisters();                                                      // Seeking will now start

    if (bWaitForSTC) {
        // Wait until STC is set.
        do { } while (!seekTuneComplete());
    }
}



int Si4703::channelToFrequency(int intChannel) {

    // Converts the "Channel" format that the Si4703  returns to Frequency in MHz x 10.
    // Eg: intFreq_MHz_x_10 = 977; // 97.7 MHz.
    //
    // Note: This function assumes you've already called readRegisters();

    // What band are we using?
    int valueBAND = getRegValue(REG_SYSCONFIG2, BAND);
    int intBottomOfBand = 875;
    if ((valueBAND == 1) || (valueBAND == 2)) intBottomOfBand = 760;

    // What spacing are we using?
    int valueSPACE = getRegValue(REG_SYSCONFIG2, SPACE);
    int intSpacing = 2; // MHz x 10
    if (valueSPACE == 1) intSpacing = 1;
    //else if (valueSPACE == 2) intSpacing = 0.5; // Not supported

    // Work out the frequency
    return intSpacing * intChannel + intBottomOfBand;
}



int Si4703::frequencyToChannel(int intFreq_MHz_x_10) {

    // Converts Frequency in MHz x 10 to the "Channel" format that the Si4703 is expecting.
    // Eg: intFreq_MHz_x_10 = 977; // 97.7 MHz.
    //
    // Note: This function assumes you've already called readRegisters();
    
    // What band are we using?
    int valueBAND = getRegValue(REG_SYSCONFIG2, BAND);
    int intBottomOfBand = 875;
    if ((valueBAND == 1) || (valueBAND == 2)) intBottomOfBand = 760;

    // What spacing are we using?
    int valueSPACE = getRegValue(REG_SYSCONFIG2, SPACE);
    int intSpacing = 2; // MHz x 10
    if (valueSPACE == 1) intSpacing = 1;
    //else if (valueSPACE == 2) intSpacing = 0.5; // Not supported

    // Work out the channel
    return (intFreq_MHz_x_10 - intBottomOfBand) / intSpacing;
}