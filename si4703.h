/* 
+----------+
| Si4703.h |
+----------+
Library for Sparkfun Si4703 breakout board.

By Andrew Wyatt
retrojdm.com

Started:        2013-09-18
Updated:        2013-09-19
Last Change:    frequencyToChannel() and channelToFrequency()


Extending the work of both:
- Simon Monk, and
- Nathan Seidle from Sparkfun


I initially just wanted to fiddle with the seek sensitivity settings, and show the channel while seeking.

After reading the datasheet I got a bit carried away.

A big thankyou goes to Nathan and Simon. I wouldn't have even attempted to use the Si4703 without their code.

Note: My code formatting and comments rely hevily on a tab width of 4 characters. Otherwise it will look messy as hell.


+------------------+
| Simon's comments |
+------------------+
Library for Sparkfun Si4703 breakout board.
Simon Monk. 2011-09-09

This is a library wrapper and a few extras to the excellent code produced
by Nathan Seidle from Sparkfun (Beerware).


+-------------------+
| Nathan's comments |
+-------------------+

Look for serial output at 57600bps.

The Si4703 ACKs the first byte, and NACKs the 2nd byte of a read.

1/18 - after much hacking, I suggest NEVER write to a register without first reading the contents of a chip.
ie, don't updateRegisters without first readRegisters.

If anyone manages to get this datasheet downloaded
http://wenku.baidu.com/view/d6f0e6ee5ef7ba0d4a733b61.html
Please let us know. It seem to be the latest version of the programming guide. It had a change on page 12 (write 0x8100 to 0x07)
that allowed me to get the chip working..

Also, if you happen to find "AN243: Using RDS/RBDS with the Si4701/03", please share. I love it when companies refer to
documents that don't exist.

1/20 - Picking up FM stations from a plane flying over Portugal! Sweet! 93.9MHz sounds a little soft for my tastes,s but
it's in Porteguese.

ToDo:
    Display current status (from 0x0A)  - done 1/20/11
    Add RDS decoding                    - works, sort of
    Volume Up/Down                      - done 1/20/11
    Mute toggle                         - done 1/20/11
    Tune Up/Down                        - done 1/20/11
    Read current channel (0xB0)         - done 1/20/11
    Setup for Europe                    - done 1/20/11
    Seek up/down                        - done 1/25/11

The Si4703 breakout does work with line out into a stereo or other amplifier. Be sure to test with different length 3.5mm
cables. Too short of a cable may degrade reception.
*/

#ifndef Si4703_h
#define Si4703_h

#include "Arduino.h"



class Si4703 {
    
    public:
        
        // +-------------------+
        // | Class Constructor |
        // +-------------------+
        Si4703(int resetPin, int sdioPin, int sclkPin);

        
        // +-------------------+
        // | Public Attributes |
        // +-------------------+
        boolean     stereo;                                 // This is updated by both setFrequency() and seekStatus();

        
        // +------------------+
        // | Public Functions |
        // +------------------+
        void        powerOn             ();                 // call in setup
        void        powerOff            ();
        void        setVolume           (int volume);       // 0 to 15
        void        setFrequency        (int freq_MHz_x10); // Eg: 977 = 97.7 Mhz
        int         getFrequency        ();
        int         seekUp              ();                 // Like originally, this waits for STC
        int         seekDown            ();
        
        // I've added these functions to
        // allow showing the frequency while tuning.
        void        startSeekingUp      ();                 // Call this when you want to seek
        void        startSeekingDown    ();
        boolean     seekTuneComplete    ();                 // Eg: while (!mySi4703.seekTuneComplete()) { print(mySi4703.getFrequency()); }
        int         seekStatus          ();                 // Eg: int intStatus = mySi4703.seekStatus();
        
        void        readRDS             (char* message, long timeout);
                                                            // message should be at least 9 chars
                                                            // result will be null terminated
                                                            // timeout in milliseconds
    private:
        
        // +------+
        // | Pins |
        // +------+
        int         _resetPin;
        int         _sdioPin;
        int         _sclkPin;


        // +--------------------+
        // | Array of Registers |
        // +--------------------+
        uint16_t    si4703_registers[16];   // There are 16 registers, each 16 words large (32 bits)


        // +-------------------+
        // | Private Functions |
        // +-------------------+
        void        si4703_init         ();
        int         getRegValue         (uint16_t intRegister, uint16_t intMask);
        int         setRegValue         (uint16_t intRegister, uint16_t intMask, uint16_t value);
        void        readRegisters       ();
        byte        updateRegisters     ();
        void        seek                (byte seekDirection, boolean bWaitForSTC);
        int         channelToFrequency  (int intChannel);
        int         frequencyToChannel  (int intFreq_MHz_x_10);
        
        
        // +-----------+
        // | Constants |
        // +-----------+
        static const uint16_t   FAIL        = 0;
        static const uint16_t   SUCCESS     = 1;

        static const int        SI4703      = 0x10; // 0b._001.0000 = I2C address of Si4703 - note that the Wire function assumes non-left-shifted I2C address, not 0b.0010.000W
        static const uint16_t   I2C_FAIL_MAX= 10;   // This is the number of attempts we will try to contact the device before erroring out
        static const uint16_t   SEEK_DOWN   = 0;    // Direction used for seeking. Default is down
        static const uint16_t   SEEK_UP     = 1;

        
        // +----------------+
        // | Register Names |
        // +----------------+
        static const uint16_t   REG_DEVICEID    = 0x00;
        static const uint16_t   REG_CHIPID      = 0x01;
        static const uint16_t   REG_POWERCFG    = 0x02;
        static const uint16_t   REG_CHANNEL     = 0x03;
        static const uint16_t   REG_SYSCONFIG1  = 0x04;
        static const uint16_t   REG_SYSCONFIG2  = 0x05;
        static const uint16_t   REG_SYSCONFIG3  = 0x06;
        static const uint16_t   REG_TEST1       = 0x07;
        static const uint16_t   REG_TEST2       = 0x08;
        static const uint16_t   REG_BOOTCONFIG  = 0x09;
        static const uint16_t   REG_STATUSRSSI  = 0x0A;
        static const uint16_t   REG_READCHAN    = 0x0B;
        static const uint16_t   REG_RDSA        = 0x0C;
        static const uint16_t   REG_RDSB        = 0x0D;
        static const uint16_t   REG_RDSC        = 0x0E;
        static const uint16_t   REG_RDSD        = 0x0F;

        
        // ----------------------------------------------------------------------------------------------------
    
        
        // +------------------------------+
        // | Register 0x00 - REG_DEVICEID |
        // +------------------------------+
        // Reset value = 0x1242

        static const uint16_t   PN          = 0xF000;// R    4-bit [15:12] Part Number.         0x01 = Si4702/03
        static const uint16_t   MFGID       = 0x0FFF;// R   12-bit [11: 0] Manufacturer ID.     0x242
        

        // ----------------------------------------------------------------------------------------------------
        

        // +----------------------------+
        // | Register 0x01 - REG_CHIPID |
        // +----------------------------+
        // Si4702C19 Reset value = 0x1053 if ENABLE = 1
        // Si4702C19 Reset value = 0x1000 if ENABLE = 0
        // Si4703C19 Reset value = 0x1253 if ENABLE = 1
        // Si4703C19 Reset value = 0x1200 if ENABLE = 0

        static const uint16_t   REV         = 0xFC00;// R    6-bit [15:10] Chip Version.        0x04 = Rev C
        
        static const uint16_t   DEV         = 0x03C0;/* R    4-bit [ 9: 6] Device.                 0 before power up = Si4702.
                                                                                                0001 after  power up = Si4072.
                                                                                                1000 before power up = Si4703.
                                                                                                1001 after  power up = Si4703. */
        
        static const uint16_t   FIRMWARE    = 0x003F;/* R    6-bit [ 5: 0] Firmware Version.    0 before power up.
                                                                                                Firmware version after power up = 010011 (-C19).
                                                    */

        // ----------------------------------------------------------------------------------------------------
        
        
        // +------------------------------+
        // | Register 0x02 - REG_POWERCFG |
        // +------------------------------+
        // Reset value 0x0000

        static const uint16_t   DSMUTE      = 0x8000;// R/W  1-bit [   15] Softmute Disable.    0 = Softmute enable (default).  1 = Softmute disable.
        static const uint16_t   DMUTE       = 0x4000;// R/W  1-bit [   14] Mute Disable.        0 = Mute enable (default).      1 = Mute disable.
        static const uint16_t   MONO        = 0x2000;// R/W  1-bit [   13] Mono Select.         0 = Stereo (default).           1 = Force mono.
        //static const uint16_t RESERVED    = 0x1000;// R/W  1-bit [   12] Reserved.            Always write to 0.
        static const uint16_t   RDSM        = 0x0800;// R/W  1-bit [   11] RDS Mode.            0 = Standard (default).         1 = Verbose (refer to "4.4. RDS/RBDS Processor and Functionality").
        static const uint16_t   SKMODE      = 0x0400;// R/W  1-bit [   10] Seek Mode.           0 = Wrap (default).             1 = Stop seeking at the upper or lower band limit.
        static const uint16_t   SEEKUP      = 0x0200;// R/W  1-bit [    9] Seek Direction.      0 = Seek down (default).        1 = Seek up.
        
        static const uint16_t   SEEK        = 0x0100;/* R/W  1-bit [    8] Seek.
                                                        
                                                    0 = Disable (default).
                                                    1 = Enable.

                                                    Notes:
                                                    1.  Seek begins at the current channel, and goes in the direction specified with the SEEKUP
                                                        bit. Seek operation stops when a channel is qualified as valid according to the seek
                                                        parameters, the entire band has been searched (SKMODE = 0), or the upper or lower
                                                        band limit has been reached (SKMODE = 1).

                                                    2.  The STC bit is set high when the seek operation completes and/or the SF/BL bit is set
                                                        high if the seek operation was unable to find a channel qualified as valid according to the
                                                        seek parameters. The STC and SF/BL bits must be set low by setting the SEEK bit low
                                                        before the next seek or tune may begin.

                                                    3.  Seek performance for 50 kHz channel spacing varies according to RCLK tolerance.
                                                        Silicon Laboratories recommends ±50 ppm RCLK crystal tolerance for 50 kHz seek
                                                        performance.

                                                    4.  A seek operation may be aborted by setting SEEK = 0.
                                                    */
        
        //static const uint16_t RESERVED    = 0x0080;// R/W  1-bit [    7] Reserved.            Always write to 0.
        static const uint16_t   DISABLE     = 0x0040;// R/W  1-bit [    6] Powerup Disable.     Refer to "4.9. Reset, Powerup, and Powerdown". Default = 0.
        //static const uint16_t RESERVED    = 0x003E;// R/W  5-bit [ 5: 1] Reserved.            Always write to 0.
        static const uint16_t   ENABLE      = 0x0001;// R/W  1-bit [    0] Powerup Enble.       Refer to "4.9. Reset, Powerup, and Powerdown". Default = 0.
        

        // ----------------------------------------------------------------------------------------------------
        
        
        // +-----------------------------+
        // | Register 0x03 - REG_CHANNEL |
        // +-----------------------------+
        // Reset value = 0x0000
        

        static const uint16_t   TUNE        = 0x8000;/* R/W  1-bit [   15] Tune.
        
                                                    0 = Disable (default).
                                                    1 = Enable.
                                                    
                                                    The tune operation begins when the TUNE bit is set high. The STC bit is set high
                                                    when the tune operation completes. The STC bit must be set low by setting the TUNE
                                                    bit low before the next tune or seek may begin.
                                                    */
        
        //static const uint16_t RESERVED    = 0x7C00;// R/W  5-bit [14:10] Reserved. Always write to 0.
        
        static const uint16_t   CHAN        = 0x03FF;/* R/W 10-bit [ 9: 0] Channel Select.
        
                                                    Channel value for tune operation.
                                                    
                                                    If BAND 05h[7:6] = 00, then Freq (MHz) = Spacing (MHz) x Channel + 87.5 MHz.
                                                    If BAND 05h[7:6] = 01, BAND 05h[7:6] = 10, then Freq (MHz) = Spacing (MHz) x Channel + 76 MHz.
                                                    
                                                    CHAN[9:0] is not updated during a seek operation. READCHAN[9:0] provides the
                                                    current tuned channel and is updated during a seek operation and after a seek or
                                                    tune operation completes. Channel spacing is set with the bits SPACE 05h[5:4].
                                                    */

        
        // ----------------------------------------------------------------------------------------------------
        
        
        // +--------------------------------+
        // | Register 0x04 - REG_SYSCONFIG1 |
        // +--------------------------------+
        // Reset value = 0x0000
        
        static const uint16_t   RDSIEN      = 0x8000;/* R/W  1-bit [   15] RDS Interrupt Enable.
        
                                                    0 = Disable (default).
                                                    1 = Enable.
                                                                                            
                                                    Setting RDSIEN = 1 and GPIO2[1:0] = 01 will generate a 5 ms low pulse on GPIO2 when
                                                    the RDSR 0Ah[15] bit is set.
                                                    */
        
        static const uint16_t   STCIEN      = 0x4000;/* R/W  1-bit [   14] Seek/Tune Complete Interrupt Enable.
        
                                                    0 = Disable (default).
                                                    1 = Enable.

                                                    Setting STCIEN = 1 and GPIO2[1:0] = 01 will generate a 5 ms low pulse on GPIO2 when
                                                    the STC 0Ah[14] bit is set.
                                                    */

        //static const uint16_t RESERVED    = 0x2000;// R/W  1-bit [   13] Reserved.            Always write to 0.
        static const uint16_t   RDS         = 0x1000;// R/W  1-bit [   12] RDS Enable.          0 = Disable (default).                  1 = Enable.
        static const uint16_t   DE          = 0x0800;// R/W  1-bit [   11] De-emphasis.         0 = 75us. Used in USA (default).        1 = 50us. Used in Europe, Australia, Japan.
        static const uint16_t   AGCD        = 0x0400;// R/W  1-bit [   10] AGC Disable.         0 = AGC enable (default).               1 = AGC disable.
        //static const uint16_t RESERVED    = 0x0300;// R/W  2-bit [ 9: 8] Reserved.            Always write to 0.

        static const uint16_t   BLNDADJ     = 0x00C0;/* R/W  2-bit [ 7: 6] Stereo/Mono Blend Level Adjustment.

                                                    Sets the RSSI range for stereo/mono blend.
                                                    
                                                    00 = 3149 RSSI dBuV (default).
                                                    01 = 3755 RSSI dBuV (+6 dB).
                                                    10 = 1937 RSSI dBuV (12 dB).
                                                    11 = 2543 RSSI dBuV (6 dB).
                                                    
                                                    ST bit set for RSSI values greater than low end of range.
                                                    */

        static const uint16_t   GPIO3       = 0x0030;/* R/W  2-bit [ 5: 4] General Purpose I/O 3.
                                                        
                                                    00 = High impedance (default).
                                                    01 = Mono/Stereo indicator (ST). The GPIO3 will output a logic high when the device is in
                                                         stereo, otherwise the device will output a logic low for mono.
                                                    10 = Low.
                                                    11 = High.
                                                    */

        static const uint16_t   GPIO2       = 0x000C;/* R/W  2-bit [ 3: 2] General Purpose I/O 2.
                                                        
                                                    00 = High impedance (default).
                                                    01 = STC/RDS interrupt. A logic high will be output unless an interrupt occurs as
                                                         described below.
                                                    10 = Low.
                                                    11 = High.
                                                    
                                                    Setting STCIEN = 1 will generate a 5 ms low pulse on GPIO2 when the STC 0Ah[14] bit is
                                                    set. Setting RDSIEN = 1 will generate a 5 ms low pulse on GPIO2 when the RDSR
                                                    0Ah[15] bit is set.
                                                    */

        static const uint16_t   GPIO1       =  0x0003;/* R/W     2-bit [ 1: 0] General Purpose I/O 1.
                                                        
                                                    00 = High impedance (default).
                                                    01 = Reserved.
                                                    10 = Low.
                                                    11 = High.
                                                    */
        
        // ----------------------------------------------------------------------------------------------------


        // +--------------------------------+
        // | Register 0x05 - REG_SYSCONFIG2 |
        // +--------------------------------+
        // Reset value = 0x0000

        static const uint16_t   SEEKTH      = 0xFF00;/* R/W  8-bit [15: 8] RSSI Seek Threshold.
                                                        
                                                    0x00 = min RSSI (default).
                                                    0x7F = max RSSI.
                                                    
                                                    SEEKTH presents the logarithmic RSSI threshold for the seek operation. The
                                                    Si4702/03-C19 will not validate channels with RSSI below the SEEKTH value.
                                                    SEEKTH is one of multiple parameters that can be used to validate channels. For
                                                    more information, see "AN284: Si4700/01 Firmware 15 Seek Adjustability and Settings."
                                                    */

        static const uint16_t   BAND        = 0x00C0;/* R/W  2-bit [ 7: 6] Band Select.
                                                        
                                                    00 = 87.5108 MHz (USA, Europe) (Default).
                                                    01 = 76108 MHz (Japan wide band).
                                                    10 = 7690 MHz (Japan).
                                                    11 = Reserved.
                                                    */

        static const uint16_t   SPACE       = 0x0030;/* R/W  2-bit [ 5: 4] Channel Spacing.
                                                        
                                                    00 = 200 kHz (USA, Australia) (default).
                                                    01 = 100 kHz (Europe, Japan).
                                                    10 =  50 kHz.
                                                    */
    
        static const uint16_t   VOLUME      = 0x000F;/* R/W  4-bit [ 3: 0] Volume.
                                                        
                                                    Relative value of volume is shifted 30 dBFS with the VOLEXT 06h[8] bit.
                                                    
                                                    VOLEXT = 0 (default).
                                                    0000 = mute (default).
                                                    0001 = 28 dBFS.
                                                    ::
                                                    1110 = 2 dBFS.
                                                    1111 = 0 dBFS.
                                                    
                                                    VOLEXT = 1.
                                                    0000 = mute.
                                                    0001 = 58 dBFS.
                                                    ::
                                                    1110 = 32 dBFS.
                                                    1111 = 30 dBFS.
                                                    FS = full scale.
                                                    Volume scale is logarithmic.
                                                    */
        
        // ----------------------------------------------------------------------------------------------------

        
        // +--------------------------------+
        // | Register 0x06 - REG_SYSCONFIG3 |
        // +--------------------------------+
        // Reset value = 0x0000
        static const uint16_t   SMUTER      = 0xC000;/* R/W  2-bit [15:14] Softmute Attack/Recover Rate.
                                                        
                                                    00 = fastest (default).
                                                    01 = fast.
                                                    10 = slow.
                                                    11 = slowest.
                                                    */

        static const uint16_t   SMUTEA      = 0x3000;/* R/W  2-bit [13:12] Softmute Attenuation.
                                                        
                                                    00 = 16 dB (default).
                                                    01 = 14 dB.
                                                    10 = 12 dB.
                                                    11 = 10 dB.
                                                    */

        //static const uint16_t RESERVED    =  0x0E00;// R/W     3-bit [11: 9] Reserved. Always write to 0.

        static const uint16_t   VOLEXT      =  0x0100;/* R/W     1-bit [    8] Extended Volume Range.
        
                                                    0 = disabled (default).
                                                    1 = enabled.
                                                    
                                                    This bit attenuates the output by 30 dB. With the bit set to 0, the 15 volume settings
                                                    adjust the volume between 0 and 28 dBFS. With the bit set to 1, the 15 volume settings
                                                    adjust the volume between 30 and 58 dBFS.
                                                    Refer to 4.5. "Stereo Audio Processing" on page 16.
                                                    */

        static const uint16_t   SKSNR       = 0x00F0;/* R/W  4-bit [ 7: 4] Seek SNR Threshold.
                                                        
                                                    0000 = disabled (default).
                                                    0001 = min (most stops).
                                                    0111 = max (fewest stops).
                                                    Required channel SNR for a valid seek channel.
                                                    */

        static const uint16_t   SKCNT       =  0x000F;/* R/W     4-bit [ 3: 0] Seek FM Impulse Detection Threshold.
                                                        
                                                    0000 = disabled (default).
                                                    0001 = max (most stops).
                                                    1111 = min (fewest stops).
                                                    Allowable number of FM impulses for a valid seek channel.
                                                    */


        // ----------------------------------------------------------------------------------------------------
        

        // +---------------------------+
        // | Register 0x07 - REG_TEST1 |
        // +---------------------------+
        // Reset value = 0x0100

        static const uint16_t   XOSCEN      = 0x8000;/* R/W  1-bit [   15] Crystal Oscillator Enable.
                                                        
                                                    0 = Disable (default).
                                                    1 = Enable.
                                                    
                                                    The internal crystal oscillator requires an external 32.768 kHz crystal as shown in
                                                    2. "Typical Application Schematic" on page 14. The oscillator must be enabled before
                                                    powerup (ENABLE = 1) as shown in Figure 9, Initialization Sequence, on page 21. It
                                                    should only be disabled after powerdown (ENABLE = 0). Bits 13:0 of register 07h
                                                    must be preserved as 0x0100 while in powerdown and as 0x3C04 while in powerup.
                                                    Refer to Si4702/03 Internal Crystal Oscillator Errata.
                                                    */

        static const uint16_t   AHIZEN      = 0x4000;/* R/W  1-bit [   14] Audio High-Z Enable.
                                                        
                                                    0 = Disable (default).
                                                    1 = Enable.
                                                    
                                                    Setting AHIZEN maintains a dc bias of 0.5 x VIO on the LOUT and ROUT pins to prevent
                                                    the ESD diodes from clamping to the VIO or GND rail in response to the output
                                                    swing of another device. Register 07h containing the AHIZEN bit must not be written
                                                    during the powerup sequence and high-Z only takes effect when in powerdown and
                                                    VIO is supplied. Bits 13:0 of register 07h must be preserved as 0x0100 while in powerdown
                                                    and as 0x3C04 while in powerup.
                                                    */

        /*static const uint16_t RESERVED    =  0x3FFF;/* R/W    14-bit [13: 0] Reserved.
                                                    
                                                    If written, these bits should be read first and then written with their pre-existing values.
                                                    Do not write during powerup.
                                                    */

        
        // ----------------------------------------------------------------------------------------------------
        

        // +---------------------------+
        // | Register 0x08 - REG_TEST2 |
        // +---------------------------+
        // Reset value = 0x0000

        /*static const uint16_t RESERVED    =  0xFFFF;/* R/W    16-bit [15: 0] Reserved.
                                                    
                                                    If written, these bits should be read first and then written with their pre-existing values.
                                                    Do not write during powerup.
                                                    */

        
        // ----------------------------------------------------------------------------------------------------



        // +--------------------------------+
        // | Register 0x09 - REG_BOOTCONFIG |
        // +--------------------------------+
        // Reset value = 0x0000

        /*static const uint16_t RESERVED    =  0xFFFF;/* R/W    16-bit [15: 0] Reserved.
                                                    
                                                    If written, these bits should be read first and then written with their pre-existing values.
                                                    Do not write during powerup.
                                                    */

        
        // ----------------------------------------------------------------------------------------------------


        
        // +--------------------------------+
        // | Register 0x0A - REG_STATUSRSSI |
        // +--------------------------------+
        // Reset value = 0x0000
        
        static const uint16_t   RDSR        = 0x8000;/* R    1-bit [   15] RDS Ready.
                                                    
                                                    0 = No RDS group ready (default).
                                                    1 = New RDS group ready.
                                                    
                                                    Refer to "4.4. RDS/RBDS Processor and Functionality".
                                                    */

        static const uint16_t   STC         = 0x4000;/* R    1-bit [   14] Seek/Tune Complete.

                                                    0 = Not complete (default).
                                                    1 = Complete.
                                                    
                                                    The seek/tune complete flag is set when the seek or tune operation completes. Setting
                                                    the SEEK 02h[8] or TUNE 03h[15] bit low will clear STC.
                                                    */

        static const uint16_t   SFBL        = 0x2000;/* R    1-bit [   13] Seek Fail/Band Limit.
                                                    
                                                    0 = Seek successful.
                                                    1 = Seek failure/Band limit reached.
                                                    
                                                    The SF/BL flag is set high when SKMODE 02h[10] = 0 and the seek operation fails to
                                                    find a channel qualified as valid according to the seek parameters.
                                                    The SF/BL flag is set high when SKMODE 02h[10] = 1 and the upper or lower band limit
                                                    has been reached.
                                                    The SEEK 02h[8] bit must be set low to clear SF/BL.
                                                    */

        static const uint16_t   AFCRL       = 0x1000;/* R    1-bit [   12] AFC Rail.
                                                    
                                                    0 = AFC not railed.
                                                    1 = AFC railed, indicating an invalid channel. Audio output is softmuted when set.
                                                    
                                                    AFCRL is updated after a tune or seek operation completes and indicates a valid or
                                                    invalid channel. During normal operation, AFCRL is updated to reflect changing RF environments.
                                                    */

        static const uint16_t   RDSS        = 0x0800;/* R    1-bit [   11] RDS Synchronized.
                                                    
                                                    0 = RDS decoder not synchronized (default).
                                                    1 = RDS decoder synchronized.
                                                    
                                                    Available only in RDS Verbose mode (RDSM 02h[11] = 1).
                                                    Refer to "4.4. RDS/RBDS Processor and Functionality".
                                                    */

        static const uint16_t   BLERA       = 0x0600;/* R    2-bit [10: 9] RDS Block A Errors.
                                                    
                                                    00 = 0 errors requiring correction.
                                                    01 = 12 errors requiring correction.
                                                    10 = 35 errors requiring correction.
                                                    11 = 6+ errors or error in checkword, correction not possible.
                                                    
                                                    Available only in RDS Verbose mode (RDSM 02h[11] = 1).
                                                    Refer to "4.4. RDS/RBDS Processor and Functionality".
                                                    */

        static const uint16_t   STEREO      =  0x0100;/* R   1-bit [    8] Stereo Indicator.
                                                    
                                                    0 = Mono.
                                                    1 = Stereo.
                                                    
                                                    Stereo indication is also available on GPIO3 by setting GPIO3 04h[5:4] = 01.
                                                    */
        
        static const uint16_t   RSSI        =  0x00FF;/* R   8-bit [ 7: 0] RSSI (Received Signal Strength Indicator).
                                                    
                                                    RSSI is measured units of dBuV in 1 dB increments with a maximum of approximately
                                                    75 dBuV. Si4702/03-C19 does not report RSSI levels greater than 75 dBuV.
                                                    */


        // ----------------------------------------------------------------------------------------------------


        // +------------------------------+
        // | Register 0x0B - REG_READCHAN |
        // +------------------------------+
        // Reset value = 0x0000
        
        static const uint16_t   BLERB       = 0xC000;// R    2-bit [15:14] RDS Block B Errors.
        static const uint16_t   BLERC       = 0x3000;// R    2-bit [13:12] RDS Block C Errors.
        static const uint16_t   BLERD       = 0x0C00;/* R    2-bit [11:10] RDS Block D Errors.
                                                    
                                                    00 = 0 errors requiring correction.
                                                    01 = 12 errors requiring correction.
                                                    10 = 35 errors requiring correction.
                                                    11 = 6+ errors or error in checkword, correction not possible.
                                                    
                                                    Available only in RDS Verbose mode (RDSM = 1).
                                                    Refer to "4.4. RDS/RBDS Processor and Functionality".
                                                    */

        static const uint16_t   READCHAN    =  0x03FF;/* R  10-bit [ 9: 0] Read Channel.
                                                    
                                                    If BAND 05h[7:6] = 00, then Freq (MHz) = Spacing (MHz) x Channel + 87.5 MHz.
                                                    If BAND 05h[7:6] = 01, BAND 05h[7:6] = 10, then Freq (MHz) = Spacing (MHz) x Channel + 76 MHz.
                                                    
                                                    READCHAN[9:0] provides the current tuned channel and is updated during a seek
                                                    operation and after a seek or tune operation completes. Spacing and channel are set
                                                    with the bits SPACE 05h[5:4] and CHAN 03h[9:0].
                                                    */      
};

#endif